package ru.appngo.fueltest

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.github.kittinunf.fuel.httpGet
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        test_text_view.text = BuildConfig.FIRST_FIELD
        if (BuildConfig.DEBUG) {
            println(BuildConfig.FIRST_FIELD)
        }
//        testFuel()
    }

    private fun testFuel() {
        "https://google.com/"
            .httpGet()
            .responseString { request, response, result ->
                println(request)
                println(response)
                println(result)
                runOnUiThread {
                    test_text_view.text = result.get()
                }
            }
    }
}
